<?php
/*
Plugin Name: Hash Resolve
Plugin URI: https://www.gitlab.com/tradworker/hash-resolve/
Description: Crowdsourced ICANN Censorship Resistance
Version: 20170920
Author: @wikitopian
Author URI: https://gitlab.com/wikitopian
Text Domain: hash-resolve
*/

require_once( 'vendor/autoload.php' );
use \PurplePixie\PhpDns\DNSQuery;

require_once( 'classes/hash-resolve-admin.php' );
require_once( 'classes/hash-resolve-thickbox.php' );

class Hash_Resolve {

	private $admin;

	private $thickbox;

	private $debug = false;

	public function __construct( $debug = false ) {
		$this->debug = $debug;

		add_action( 'init', array( &$this, 'do_register_style' ) ); // used both front and dash

		$this->admin = new Hash_Resolve_Admin();

		add_action( 'wp_ajax_hash-resolve',        array( &$this, 'do_hash_resolve' ) );
		add_action( 'wp_ajax_nopriv_hash-resolve', array( &$this, 'do_hash_resolve' ) );

		// Only function on the home page
		if ( $_SERVER['REQUEST_URI'] == '/' ) {
			add_action( 'wp_enqueue_scripts', array( &$this, 'do_script' ) );

			$this->thickbox = new Hash_Resolve_Thickbox();
		}

	}

	public static function do_install() {

		$settings = get_option( 'hash-resolve' );

		if( empty( $settings ) ) {

			$settings = [
				'alt'   => '192.52.166.110',
				'proxy' => 'http://proxy.opennicproject.org/proxy.php?q=[domain]&hl=e1',
				'onion' => 'https://[domain].link/',
			];

			update_option( 'hash-resolve', $settings );
		}

	}

	public function do_register_style() {

		$cache_bust = '';
		if( $this->debug ) {
			$cache_bust = '?cache=' . rand( 0, 9001 );
		}

		wp_register_style(
			'hash-resolve',
			plugin_dir_url( __FILE__ ) . '/styles/hash-resolve.css' . $cache_bust,
			array( 'thickbox' )
		);

	}

	public function do_script() {

		$cache_bust = '';
		if( $this->debug ) {
			$cache_bust = '?cache=' . rand( 0, 9001 );
		}

		wp_enqueue_script(
			'hash-resolve',
			plugin_dir_url( __FILE__ ) . '/scripts/hash-resolve.js' . $cache_bust,
			array( 'jquery', 'thickbox' ),
			'20170918',
			true
		);

		$nonce = wp_create_nonce( 'hash-resolve' );

		$settings = get_option( 'hash-resolve' );

		$hash_resolve = array(
			'nonce' => $nonce,
			'plugin_dir' => plugin_dir_url( __FILE__ ),
			'ajaxurl'    => admin_url( 'admin-ajax.php' ),
			'proxy'      => $settings['proxy'],
			'onion'      => $settings['onion'],
		);

		wp_localize_script(
			'hash-resolve',
			'hash_resolve',
			$hash_resolve
		);

		wp_enqueue_style( 'hash-resolve' );

	}

	public function do_hash_resolve() {

		check_ajax_referer( 'hash-resolve', 'nonce' );

		if( !isset( $_REQUEST['hash'] ) ) {
			wp_die();
		}
		$hash = strtolower( $_REQUEST['hash'] );

		$domain_parts = null;
		$is_domain = preg_match('/^(.*\\.)?([^\\.]+)\\.([^\\.]{2,6})$/', $hash, $domain_parts );

		$subdomain = $domain_parts[1];
		$domain    = $domain_parts[2];
		$tld       = $domain_parts[3];

		if( !$is_domain ) {
			$response['valid'] = 0;
		} else {
			$response['valid'] = 1;
			$response['domain'] = "{$subdomain}{$domain}.{$tld}";

			$resolved = $this->do_resolve( "{$domain}.{$tld}" );

			if( empty( $resolved['ip'] ) ) {
				$response['valid'] = 0;
			} else {
				$response['ip']   = $resolved['ip'];
				$response['tld']  = $resolved['tld'];
				$response['alts'] = $resolved['alts'];
			}

		}

		echo json_encode( $response );

		wp_die();
	}

	public function do_resolve( $address ) {

		if( preg_match( '/\.onion$/', $address ) ) {
			$resolved = array();
			$resolved['ip'] = '0.0.0.0';
			$resolved['domain'] = $address;
			$resolved['valid'] = 1;
			$resolved['alts'] = array();
			$resolved['tld'] = 'onion';

			return $resolved;
		}

		$settings = get_option( 'hash-resolve' );
		$resolved = array();

		/* ICANN DNS Lookup for SRV Records */

		$tlds = ['onion', 'bit'];

		$alts = array();
		foreach( $tlds as $tld ) {
			$result = dns_get_record( '_dns.' . $tld .  '._tcp.' . $address, DNS_SRV );

			if( !empty( $result ) ) {
				$alts[$tld] = $result[0]['target'];
			}
		}
		$resolved['alts'] = $alts;

		/* Alt.DNS Lookup */

		if( !filter_var( $settings['alt'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			if( $this->debug ) {
				error_log( 'Hash Resolve: Invalid Alt.DNS IP' );
			}
		}

		try {

			if ( false === ( $result = get_transient( 'hash-resolve_' . $address ) ) ) {
				$dns_query=new DNSQuery( $settings['alt'] );
				$result = $dns_query->Query( $address, 'A' );
			}

			set_transient( 'hash-resolve_' . $address, $result, 60 * 60 * 1 ); // one hour DNS cache

			if( $result === false || empty( $result->results ) ) {
				throw new Exception( 'Lookup Failure: ' . $address );
			}

			$resolved['ip'] = $result->results[0]->getData();

			$domain = $result->results[0]->getDomain();
			$tld = preg_replace( '/[^\.]+\.(.*)/', '$1', $domain );
			$resolved['tld'] = $tld;

			return $resolved;

		} catch( Exception $e ) {
			if( $this->debug ) {
				error_log( $e->getMessage() );
			}
		}

	}

}

register_activation_hook( __FILE__, array( 'Hash_Resolve', 'do_install' ) );

$hash_resolve = new Hash_Resolve( true ); // paraemeter sets debugging

/* EOF */
