# Hash Resolve

Hash Resolve delivers Crowdsourced ICANN Censorship Resistance to end users
without the need for browser plugins or modifications to their network settings.

## Getting Started

This is a standard WordPress plugin. Once it's installed, users will be able to
rely on your site to resolve censored addresses by visiting
`yoursite.com/#bannedsite.bit` in their browser.

### Prerequisites

* [WordPress](https://www.wordpress.org/)

These two are necessary if installing from the source repo. They're not necessary
if you're unzipping a prepared zipfile into the `plugins/` folder or relying
on WordPress.org's official plugin repository.

* [Git](https://git-scm.com/)

* [PHP Composer](https://getcomposer.org/)

### Installing

To install it from the GitLab source repo (Ubuntu):

`sudo apt-get install git composer`

`cd ~/public_html/wp-content/plugins`

`git clone https://gitlab.com/tradworker/hash-resolve.git`

`composer install`

Then activate the plugin in the Dashboard and (optionally) change the default
Alt.DNS server IP.

## Built With

* [DNS API / Library for PHP](https://github.com/purplepixie/phpdns) - DNS Client
* [OpenNIC](https://www.openNIC.org/) - The OpenNIC Project
* [WordPress](https://www.wordpress.org/)

## Authors

* **Matt Parrott** - *Initial work* - [@wikitopian](https://gitlab.com/wikitopian)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Apollo Slater](https://twitter.com/apolloslater) - This plugin is inspired by
his [Hashtag Name System](https://docs.google.com/document/d/1hpMWyT3b_4UOetnozWEnvykXG_f0LyTvxzsawRuFKSk)
white paper.


## Donate

Help support the ongoing improvement of this important project.

Bitcoin: [1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB](bitcoin:1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB)
