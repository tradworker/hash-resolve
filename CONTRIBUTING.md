# Contributing

This project is apolitical, and fully open to anybody and everybody who wishes
to contribute.

## Contributing Code

I attempt to abide by the [WordPress Coding Standards](https://make.wordpress.org/core/handbook/best-practices/coding-standards/),
and may pedantically tweak your improvements to align with my interpretation of
those standards.