<?php

class Hash_Resolve_Thickbox {

	public function __construct() {

		add_action( 'wp_footer', array( &$this, 'do_peerbox' ) );

	}

	public function do_peerbox() {

		$logo = plugin_dir_url( __FILE__ ) . '../images/hash-resolve.png';

		$bitcoin_image = plugin_dir_url( __FILE__ ) . '../images/hash-resolve-donate.png';

		echo <<<PEERBOX

<div id="hash-resolve_peerbox" style="display:none">

	<div id="hash-resolve_logo">
		<img src="{$logo}" alt="Hash Resolve" title="Hash Resolve" />
	</div>

	<h3 id="hash-resolve_address"></h3>

	<div id="hash-resolve_options">

		<div id="hash-resolve_bit">
			<a class="hash-resolve_icon" target="_blank"><img /></a>
			<a class="hash-resolve_link" target="_blank"></a>
			<br />
			<a class="hash-resolve_proxy" target="_blank">[proxy]</a>
		</div>

		<div id="hash-resolve_onion">
			<a class="hash-resolve_icon" target="_blank"><img /></a>
			<a class="hash-resolve_link" target="_blank"></a>
			<br />
			<a class="hash-resolve_proxy" target="_blank">[proxy]</a>
		</div>

		<div id="hash-resolve_proxy">
			<a class="hash-resolve_icon" target="_blank"><img /></a>
			<a class="hash-resolve_link" target="_blank"></a>
		</div>

		<div id="hash-resolve_browser">
			<a class="hash-resolve_icon" target="_blank"><img /></a>
			<a class="hash-resolve_link" target="_blank"></a>
		</div>

		<div id="hash-resolve_learn">
			<a class="hash-resolve_icon" target="_blank"><img /></a>
		</div>

	</div>

	<div id="hash-resolve_about">

		<h3>Crowdsourced ICANN Censorship Resistance</h3>

		<a href="javascript:hash_resolve_about();">About Hash Resolve</a>

		<div>

			<div id="hash-resolve_about-bitcoin">

				<h4>Support This Project</h4>

				<a href="bitcoin:1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB"><img src="{$bitcoin_image}" alt="Donate" title="Donate" /></a>
				<tt><a href="bitcoin:1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB">1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB</a></tt>
			</div>

			<div id="hash-resolve_about-details">

				<h4><a href="https://gitlab.com/tradworker/hash-resolve" target="_new">Hash Resolve Project</a></h4>

				<p>
					Do you run a WordPress website? If so, consider installing
					the Hash Resolve plugin so that more sites are available
					to help fight Internet censorship and monopolistic business practices.
					The Root DNS servers are the weakest link in the web's infrastructure
					and together we can help end users more quickly and easily
					work around ICANN's dangerous monopoly on Internet speech.
				</p>

			</div>

		</div>
	</div>

</div>

PEERBOX;

	}

}

/* EOF */
