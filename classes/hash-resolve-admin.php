<?php

class Hash_Resolve_Admin {

	public function __construct() {

		if( isset( $_REQUEST['hash-resolve']['nonce'] ) ) {
			add_action( 'admin_init', array( &$this, 'do_save_option' ) );
		}

		add_action( 'admin_init', array( &$this, 'do_settings' ) );
		add_action( 'admin_menu', array( &$this, 'do_menu' ) );

		add_action( 'admin_enqueue_scripts', array( &$this, 'do_scripts' ) );

	}

	public function do_scripts( $hook ) {

		if( !is_admin() ) {
			return;
		}

		if( $hook != 'settings_page_hash-resolve' ) {
			return;
		}

		wp_enqueue_style( 'hash-resolve' );

	}

	public function do_save_option() {

		if( !wp_verify_nonce( $_REQUEST['hash-resolve']['nonce'], 'hash-resolve' ) ) {
			return;
		}

		$settings = get_option( 'hash-resolve' );

		$settings['alt']    = $_REQUEST['hash-resolve']['alt'];
		$settings['proxy']  = $_REQUEST['hash-resolve']['proxy'];
		$settings['onion']  = $_REQUEST['hash-resolve']['onion'];

		update_option( 'hash-resolve', $settings );
	}

	public function do_settings() {

		register_setting( 'hash-resolve', 'hash-resolve' );

	}

	public function do_menu() {

		add_options_page(
			'Hash Resolve',
			'Hash Resolve',
			'manage_options',
			'hash-resolve',
			array( &$this, 'do_menu_page' )
		);

	}

	public function do_menu_page() {

		$settings = get_option( 'hash-resolve' );

		$nonce = wp_nonce_field( 'hash-resolve', 'hash-resolve[nonce]', true, false );

		echo <<<HEAD
<div class="wrap">
	<h1 class="wp-heading-inline">Hash Resolve</h1>

	<hr />

	Please visit <a href="https://servers.opennicproject.org/" target="_blank">OpenNIC's Public Server Page</a>
	and then copy the IPv4 address of an Alt.DNS server which is in or nearest your audience's
	country of origin and active. Do not leave the default IP address.

	<form method="post">

		{$nonce}

		<?php settings_fields( 'hash-resolve' ); ?>
		<?php do_settings_sections( 'hash-resolve' ); ?>

		<table class="form-table">

			<tr valign="top">
			<th scope="row">Alt.DNS Server</th>
			<td><input type="text" name="hash-resolve[alt]" value="{$settings['alt']}" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Alt.DNS Proxy</th>
			<td><input type="text" size="75" name="hash-resolve[proxy]" value="{$settings['proxy']}" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Onion Proxy</th>
			<td><input type="text" size="75" name="hash-resolve[onion]" value="{$settings['onion']}" /></td>
			</tr>

		</table>

HEAD;

		submit_button();

		$bitcoin_image = plugin_dir_url( __FILE__ ) . '../images/hash-resolve-donate.png';

		echo <<<FOOT

	</form>

	<hr />

	<div id="hash-resolve_about">

		<h3>Crowdsourced ICANN Censorship Resistance</h3>

		<div>

			<div id="hash-resolve_about-bitcoin">

				<h4>Support This Project</h4>

				<a href="bitcoin:1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB"><img src="{$bitcoin_image}" alt="Donate" title="Donate" /></a>
				<tt><a href="bitcoin:1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB">1DqZu7J8PT2sy277FaJDKk4R5gw3Y2MbaB</a></tt>
			</div>

			<div id="hash-resolve_about-details">

				<h4><a href="https://gitlab.com/tradworker/hash-resolve" target="_new">Hash Resolve Project</a></h4>

				<p>

					Hash Resolve is a user interface which helps people take
					advantage of several great tools and services which deserve
					your support for helping keep the Internet truly free.

					<br /><br />

					<strong>Acknowledgments</strong><br /><br />

					<strong>
						<a href="https://www.opennic.org/">OpenNIC</a>
						<a href="bitcoin:1yM2wmPrRLSXkatn5oFLjhpXzTwDvNCpp">[Donate]</a>
					</strong> -
					An organization of hobbyists who run an alternative DNS network

					<br /><br />

					<strong>
						<a href="https://www.namecoin.info/">Namecoin</a>
						<a href="https://www.namecoin.org/donate/">[Donate]</a>
					</strong> -
					The first blockchain-driven Alt.DNS solution

					<br /><br />

					<strong>
						<a href="https://www.eff.org/">Electronic Frontier Foundation</a>
						<a href="https://www.eff.org/donate/">[Donate]</a>
					</strong> -
					The leading nonprofit organization defending civil liberties in the digital world

					<br /><br />

					<strong>
						<a href="https://www.torproject.org/">Tor Project</a>
						<a href="https://donate.torproject.org/">[Donate]</a>
					</strong> -
					A group of volunteer-operated servers that allows people to improve their privacy and security on the Internet

					<br /><br />

					<strong>
						My Wife
					</strong> -
					Tolerates my obessive hobby projects

				</p>

			</div>

		</div>
	</div>


</div>

FOOT;

	}

}

/* EOF */
