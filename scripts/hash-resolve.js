jQuery(document).ready(function( $ ) {

	$('#hash-resolve_about > div').hide();
	$('#hash-resolve_onion').hide();
	$('#hash-resolve_bit').hide();

	var hash = window.location.hash.substr(1);

	if( hash.length < 1 ) {
		return;
	}

	var data = {
		'action': 'hash-resolve',
		'nonce':  window['hash_resolve']['nonce'],
		'hash':   hash,
	};

	$.post(
		window['hash_resolve']['ajaxurl'],
		data,
		function( response ) {
			response = JSON.parse( response );

			if( !response['valid'] ) {
				console.log( 'Hash Resolve: invalid domain entry' );
				return;
			}

			var onion_link_pattern = window['hash_resolve']['onion'];
			onion_link = onion_link_pattern.replace( /\[domain]/, response['domain'] );

			var proxy_link_pattern = window['hash_resolve']['proxy'];
			proxy_link = proxy_link_pattern.replace( /\[domain]/, response['domain'] );

			// for mobile, skip the popup and directly forward
			if( $(window).width() <= 700 ) {

				if( response['domain'].includes('.onion') ) {
					window.location.href = onion_link;
				} else {
					window.location.href = proxy_link;
				}

				return;
			}

			$('#hash-resolve_address').text( 'Visit: ' + response['domain'] );

			if( response['domain'].includes('.onion') ) {

				var browser_link = "https://www.torproject.org/projects/torbrowser.html.en";

				$('#hash-resolve_proxy .hash-resolve_icon').attr( 'href', onion_link );
				$('#hash-resolve_proxy .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-onion-proxy.png' );

				$('#hash-resolve_browser .hash-resolve_icon').attr( 'href', browser_link );
				$('#hash-resolve_browser .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-onion-browser.png' );

				var learn_link = 'https://www.torproject.org';
				var tld_name = 'Tor Project';

				$('#hash-resolve_learn .hash-resolve_icon').attr( 'href', learn_link );
				$('#hash-resolve_learn .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-onion.png' );

				$('#hash-resolve_learn .hash-resolve_link').attr( 'href', learn_link );
				$('#hash-resolve_learn .hash-resolve_link').text( 'Learn More about ' + tld_name );


			} else {

				var browser_link = "https://peername.com/browser-extension/";

				$('#hash-resolve_proxy .hash-resolve_icon').attr( 'href', proxy_link );
				$('#hash-resolve_proxy .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-opennic-proxy.png' );

				$('#hash-resolve_browser .hash-resolve_icon').attr( 'href', browser_link );
				$('#hash-resolve_browser .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-peername.png' );

				switch( response['tld'] ) {

					case 'bit':
						var learn_link = 'https://www.namecoin.info';
						var tld_name = 'NameCoin';
						var learn = 'bit';
						peername = true;
						break;

					case 'bazar':
					case 'coin':
					case 'emc':
					case 'lib':
					case 'emc':
						var learn_link = 'https://www.emercoin.com';
						var tld_name = 'EmerCoin';
						var learn = 'emercoin';
						peername = true;
						break;

					default:
						var learn_link = 'https://www.opennic.org';
						var tld_name = 'OpenNIC';
						var learn = 'opennic';
						var peername = false;
						break;

				}

				if( !peername ) {
					$('#hash-resolve_browser').hide();
				}

				if( response['alts']['onion'] !== undefined  ){
					$('#hash-resolve_onion').show();

					$('#hash-resolve_onion .hash-resolve_icon').attr( 'href', 'http://' + response['alts']['onion'] );
					$('#hash-resolve_onion .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-onion-link.png' );

					$('#hash-resolve_onion .hash-resolve_link').attr( 'href', 'http://' + response['alts']['onion'] );
					$('#hash-resolve_onion .hash-resolve_link').text( response['alts']['onion'] );

					var alt_onion_link = onion_link_pattern.replace( /\[domain]/, response['alts']['onion'] );
					$('#hash-resolve_onion .hash-resolve_proxy').attr( 'href', alt_onion_link );

				}

				if( response['alts']['bit'] !== undefined  ){
					$('#hash-resolve_bit').show();

					$('#hash-resolve_bit .hash-resolve_icon').attr( 'href', 'http://' + response['alts']['bit'] );
					$('#hash-resolve_bit .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-bit-link.png' );

					$('#hash-resolve_bit .hash-resolve_link').attr( 'href', 'http://' + response['alts']['bit'] );
					$('#hash-resolve_bit .hash-resolve_link').text( response['alts']['bit'] );

					var alt_bit_link = proxy_link_pattern.replace( /\[domain]/, response['alts']['bit'] );
					$('#hash-resolve_bit .hash-resolve_proxy').attr( 'href', alt_bit_link );

				}

				$('#hash-resolve_learn .hash-resolve_icon').attr( 'href', learn_link );
				$('#hash-resolve_learn .hash-resolve_icon img').attr( 'src', window['hash_resolve']['plugin_dir'] + 'images/icon-' + learn + '.png' );

				$('#hash-resolve_learn .hash-resolve_link').attr( 'href', learn_link );

			}

			var tb_link = '#TB_inline?inlineId=hash-resolve_peerbox';
			tb_show( 'Hash Resolve', tb_link );

		}
	);

});

function hash_resolve_about() {
	jQuery('#hash-resolve_about > div').toggle();
}
